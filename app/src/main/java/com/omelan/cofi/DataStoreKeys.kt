package com.omelan.cofi

import androidx.datastore.preferences.core.preferencesKey

val PIP_ENABLED = preferencesKey<Boolean>("pip_enabled")